import { Component, OnInit, Input } from '@angular/core';

// classes
import  { User } from '../models/user';
import  { Metrics } from '../models/metrics';

@Component({
  selector: 'app-left-column',
  templateUrl: './left-column.component.html',
  styleUrls: ['./left-column.component.css']
})
export class LeftColumnComponent implements OnInit {
	@Input() user: User;
	saldo: string;
	gesendet: string;
	uberfallig: string;

  	constructor() {}
  	ngOnInit() {
  		this.saldo = this.user.saldo.toLocaleString('de-DE', { minimumFractionDigits: 3 });
  		this.gesendet = this.user.gesendet.toLocaleString('de-DE', { minimumFractionDigits: 3 });
  		this.uberfallig = this.user.uberfallig.toLocaleString('de-DE', { minimumFractionDigits: 3 });
  	}
}
