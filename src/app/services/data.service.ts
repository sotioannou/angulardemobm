import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

// classes(models)
import  { User } from '../models/user';
import  { Metrics } from '../models/metrics';

@Injectable()
export class DataService {
	private url: string = './assets/Input.json';

  constructor(private http: HttpClient) { }
  fetchData(): Observable <User> {
  	return this.http.get(this.url);
  }
}
