import { Component, OnInit } from '@angular/core';

// services
import { DataService } from './services/data.service';

// classes
import  { User } from './models/user';
import  { Metrics } from './models/metrics';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	user: User;
	constructor(private dataService: DataService) {	}
	ngOnInit() {
		this.dataService.fetchData().subscribe(response => {
			this.user = new User(response['name'], response['saldo'], response['gesendet'], response['uberfallig'], response['metrics']);
			this.user.sortMetricsAscending();
		});  	
  }
}
