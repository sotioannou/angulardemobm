import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

// services
import { DataService } from '../services/data.service';

// classes(models)
import  { User } from '../models/user';
import  { Metrics } from '../models/metrics';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  pieColorScheme = {domain: ['#F06C25', '#6B7988', '#2FC0D2']};
  // line, area
  pieAutoScale = true;

  // options
  gradient = false;
  showLegend = false;

  colorScheme = {
    domain: ['#2FC0D2']
  };

	@Input() user: User;
	saldo: string;
	metricsColors: object[] = [];
	metricsYears: object[] = [];
	metricYearsTemp: object[] = [];

  constructor(private dataService: DataService) {}

  ngOnInit() {
	 	this.saldo = this.user.saldo.toLocaleString('de-DE', { minimumFractionDigits: 3 });
		this.metricsColors = [{"name": "Orange","value": this.user.metrics[0].orange_metric}, {"name": "Gray","value": this.user.metrics[0].gray_metric},{"name": "Blue","value": this.user.metrics[0].blue_metric}];
		// push to temp first
		this.user.metrics.forEach(entry => {
			this.metricYearsTemp.push({ "name": entry.year.toFixed(), "value": entry.percent});
		});
		this.metricsYears = this.metricYearsTemp;
  }
  // event listener
  onSelect(event) {
  	let tmpMetrics: Metrics[];

  	tmpMetrics = this.user.metrics.filter(entry => {
  		if (entry['year'] == event['name']) { return entry;}
  	});
  	// add new values
  	this.metricsColors = [
  		{
  			"name": 'Orange',
  			"value": tmpMetrics[0].orange_metric
  		},
  		{
  			"name": 'Gray',
  			"value": tmpMetrics[0].gray_metric
  		},
  		{
  			"name": 'Blue',
  			"value": tmpMetrics[0].blue_metric
  		}
  	];
  }
}
