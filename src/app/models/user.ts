import { Metrics } from "./metrics";

export class User {
	constructor(private name: string, public saldo: number, public gesendet: number, public uberfallig: number,
		public metrics: Metrics[]) {};
	sortMetricsAscending() {
		this.metrics.sort((a, b) => {
			return a['year'] - b['year'];
		});
	}
}
