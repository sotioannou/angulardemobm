export class Metrics {
	constructor(public year:number, public percent: number, private units: number, private grow_units: number,
		public orange_metric: number, public gray_metric: number, public blue_metric: number){}
}
